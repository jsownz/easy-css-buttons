# Easy CSS Buttons

Just some simple buttons - their interactions are all css, don't have to worry about javascript classes ontouch and stuff, they just work.

https://www.frozensoliddesigns.com/buttons/ - see it in action

Mixin:
```sass
@mixin button ($color-bg, $color-text, $scale-down: .95) {
  font-size: 1em;
  letter-spacing: 1px;
  padding: 1em 2em;
  color: $color-text;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 2px;
  background: $color-bg;
  border: none;
  box-shadow: 0 0 20px rgba(0,0,0,.3);
  transition: all 300ms ease;
  outline: none;
  font-family: Verdana, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  &:active {
    box-shadow: 0 0 20px rgba(0,0,0,.6);
    transform: scale3d($scale-down,$scale-down,$scale-down);
  }
}
```

Maybe I'll customize the mixin some more - not sure if I want to make everything a variable cause that gets annoying.
Anyway, here's the normal css use :

```html
<button class="button green">Button</button>
```

```css
.button {
  font-size: 1em;
  letter-spacing: 1px;
  padding: 1em 2em;
  color: white;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 2px;
  background: rgba(255, 255, 255, 0.1);
  border: none;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
  transition: all 300ms ease;
  outline: none;
  font-family: Verdana, 'Helvetica Neue', Helvetica, Arial, sans-serif; }
  .button:active {
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.6);
    transform: scale3d(0.95, 0.95, 0.95); }
  .button.white {
    background: rgba(255, 255, 255, 0.8);
    color: #353535; }
  .button.green {
    background: rgba(79, 197, 130, 0.7); }
  .button.blue {
    background: rgba(67, 143, 193, 0.7); }
  .button.purple {
    background: rgba(146, 83, 198, 0.7); }
  .button.red {
    background: rgba(195, 75, 85, 0.7); }
```

And here's the use with the mixin:

```html
<button class="whatever-you-want">Button</button>
```

```sass
.whatever-you-want {
  @include button(#40BFB7, #f0f0f0, .4);
}
```